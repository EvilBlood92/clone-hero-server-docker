FROM debian:buster-slim AS build-env
ENV DEBIAN_FRONTEND=noninteractive
WORKDIR /clonehero
RUN apt-get update \
 && apt-get install --no-install-recommends -y ca-certificates wget unzip curl jq libicu63 \
 && rm -rf /var/lib/apt/lists/* \
 && mkdir config

COPY ./startup.sh .
COPY ./server-settings.ini ./config/

RUN  curl -s https://api.github.com/repos/clonehero-game/releases/releases/latest | grep "browser_download_url.*-standalone_server.zip" | cut -d : -f 2,3 | tr -d \" | wget -O chserver.zip -q -i -
RUN unzip chserver.zip
RUN rm ./chserver.zip
RUN mv ./ChStandaloneServer-* ./chserver
RUN mv ./chserver/linux-x64 ./chserver/linux-x86_64
RUN mv ./chserver/linux-arm64 ./chserver/linux-aarch64
RUN mv ./chserver/linux-$(arch)/* .
RUN rm -rf ./chserver
RUN chmod +x ./Server
RUN chown -R 1000 ./config

FROM debian:buster-slim

RUN apt-get update \
 && apt-get install --no-install-recommends -y ca-certificates libicu63 libgssapi-krb5-2 \
 && rm -rf /var/lib/apt/lists/* \
 && ln -sf /usr/src/clonehero/Server /usr/bin/cloneheroserver \
 && useradd -m clonehero

WORKDIR /usr/src/clonehero
COPY --from=build-env /clonehero .
USER clonehero

WORKDIR /usr/src/clonehero/config

EXPOSE 14242/udp
ENTRYPOINT ["../startup.sh"]
